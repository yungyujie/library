package util;

import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.junit.Test;
import java.sql.SQLException;
//数据源的封装
public class JDBCUtiles {
	// 创建c3p0连接池对象
	private static final ComboPooledDataSource dataSource = new ComboPooledDataSource();

	//获得c3p0连接池对象
	public static DataSource getDataSource() {
	    return dataSource;
	}
	
	//获得QueryRunner对象
	public static QueryRunner getQueryRunner() {
	    return new QueryRunner(dataSource);
	}
    //此部分内容来测试数据源
	@Test
	public void test() throws SQLException {
           System.out.println(dataSource.getConnection().toString());
	}
}