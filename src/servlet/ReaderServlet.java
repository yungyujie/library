package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDao;
import dao.ReaderDao;
import dao.impl.BookDaoImpl;
import dao.impl.ReaderDaoImpl;
import model.Book;
import model.BorrowBook;
import model.Reader;
import java.text.SimpleDateFormat;
import util.DateConvert;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/ReaderServlet")
public class ReaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReaderServlet() {
        super();
        // TODO Auto-generated constructor stub
 
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("utf-8");
		String action = request.getParameter("action");
		try {
		if(action.equals("register")) {			
				register(request,response);
			}else if(action.equals("login")){
				login(request,response);
			}else if(action.equals("bookList")) {
				bookList(request,response);
			}else if(action.equals("back")) {
				back(request,response);
			}else if(action.equals("booking")) {
				booking(request,response);
			}else if(action.equals("search")) {
				search(request,response);
			}else if(action.equals("bookState")) {
				bookState(request,response);
			}else if(action.equals("readingList")) {
				readingList(request,response);
			}else if(action.equals("renew")) {
				renew(request,response);
			}else if(action.equals("return")) {
				returnBook(request,response);
			}else if(action.equals("returnState")) {
				returnState(request,response);
			}else if(action.equals("returned")) {
				returned(request,response);
			}else if(action.equals("resetList")) {
				resetList(request,response);
			}else if(action.equals("readerReset")) {
				readerReset(request,response);
			}else if(action.equals("resetPwd")) {
				resetPwd(request,response);
			}else if(action.equals("delReader")) {
				delReader(request,response);
			}else if(action.equals("wantSay")) {
				wantSay(request,response);
			}else if(action.equals("wantRv")) {
				wantRv(request,response);
			}else if(action.equals("review")) {
				review(request,response);
			}else if(action.equals("comReview")) {
				comReview(request,response);
			}else if(action.equals("searchReview")) {
				searchReview(request,response);
			}else if(action.equals("myReview")) {
				myReview(request,response);
			}
		}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	
	//查看我的书评
	private void myReview(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		ReaderDao rd = new ReaderDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("myRe", rd.queryReviewById(uid));
		response.sendRedirect("myReview.jsp");
	}

	//搜索评论
	private void searchReview(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("bookName");
		BookDao bd = new BookDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("searchRe", bd.searchReview(name));
		response.sendRedirect("search-review.jsp");
	}

	//查看大家的评论
	private void comReview(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		ReaderDao rd = new ReaderDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("comRe", rd.queryAllReview());
		response.sendRedirect("comReview.jsp");
	}

	//评论图书功能
	private void review(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		int grade = Integer.parseInt(request.getParameter("grade"));
		String desc = request.getParameter("desc");
		String uid = request.getParameter("uid");
		String bid = request.getParameter("bid");
		ReaderDao rd = new ReaderDaoImpl();
		if(rd.review(bid, uid, grade, desc)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//将值传到review.jsp	
	private void wantRv(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("name");
		String bid = request.getParameter("bid");
		BorrowBook bb = new BorrowBook();
		bb.setBookName(name);
		bb.setISBN(bid);
		HttpSession session = request.getSession();
		session.setAttribute("bookname", bb);
		response.sendRedirect("review.jsp");
		
	}

	//评价列表
	private void wantSay(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		ReaderDao rd = new ReaderDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("wantSay", rd.queryReturnedById(uid));
		response.sendRedirect("wantToSay.jsp");
	}

	//注销用户
	private void delReader(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String pass = request.getParameter("pass");
		ReaderDao rd = new ReaderDaoImpl();
		if(rd.delReaderByIdAndPwd(uid, pass)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//修改密码
	private void resetPwd(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String uid = request.getParameter("uid");
		String newPass = request.getParameter("newpass");
		String pass = request.getParameter("pass");
		ReaderDao rd = new ReaderDaoImpl();
		if(rd.resetPwd(uid, pass, newPass)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//修改个人信息
	private void readerReset(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String name = request.getParameter("username");
		String dept = request.getParameter("dept");
		int phone = Integer.parseInt(request.getParameter("phone"));
		ReaderDao rd = new ReaderDaoImpl();
		if(rd.reset(uid, name, dept, phone)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//修改个人信息的列表 
	private void resetList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		ReaderDao rd = new ReaderDaoImpl();
		Reader reader = rd.getReaderById(uid);
		if(reader != null) {
			HttpSession session = request.getSession();
			session.setAttribute("resetList", reader);
			response.sendRedirect("resetList.jsp");	
		}else {
			response.sendRedirect("false.jsp");
		}			
	}

	//查看已还图书
	private void returned(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		ReaderDao rd = new ReaderDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("returnedBook", rd.queryReturnedById(uid));
		response.sendRedirect("returned-book.jsp");
	}

	//用户查看还书状态
	private void returnState(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		ReaderDao rd = new ReaderDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("returnState", rd.queryReturnStateById(uid));
		response.sendRedirect("return-state.jsp");
	}

	//用户还书
	private void returnBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String bid = request.getParameter("bid");
		ReaderDao rd = new ReaderDaoImpl();
		if(rd.returnByid(uid, bid)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//续期功能
	private void renew(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String bid = request.getParameter("bid");
		BookDao bd = new BookDaoImpl();
		if(bd.renewById(uid, bid)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//正在阅读的书单
	private void readingList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid =request.getParameter("uid");
		BookDao bd = new BookDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("state", bd.queryReadingListByid(uid));
		response.sendRedirect("reading-list.jsp");
	}

	//借书状态
	private void bookState(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid =request.getParameter("uid");
		BookDao bd = new BookDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("state", bd.queryBorrowById(uid));
		response.sendRedirect("state.jsp");
	}

	//搜索图书
	private void search(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("bookName");
		BookDao bd = new BookDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("searchBook", bd.queryBookByName(name));
		response.sendRedirect("searchList.jsp");
		
	}

	//订阅图书方法
	private void booking(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String bid = request.getParameter("bid");
		BookDao bd = new BookDaoImpl();
		if(bd.bookingById(uid, bid)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
		
	}

	//退出登录
	private void back(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		request.getSession().invalidate();
		response.sendRedirect("login.jsp");
	}

	//列出所有的图书信息
	private void bookList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub		
		BookDao bd = new BookDaoImpl();		
		HttpSession session = request.getSession();
		session.setAttribute("bookList", bd.queryAllBook());
		response.sendRedirect("book-list.jsp");
	}

	//登录方法
	private void login(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		String pwd = request.getParameter("password");
		ReaderDao rd = new ReaderDaoImpl();
		Reader reader = rd.getReaderByIdAndPwd(id, pwd);
		if(reader != null) {
			HttpSession session = request.getSession();
			session.setAttribute("reader", reader);
			response.sendRedirect("home.jsp");
		}else {
			response.sendRedirect("login.jsp");
		}
	}

	//用户注册
	private void register(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("userid");
		String usertype = request.getParameter("type");
		String name = request.getParameter("username");
		String pwd = request.getParameter("password");
		String dept = request.getParameter("dept");
		int phone = Integer.parseInt(request.getParameter("phone"));
		String ut = null ;
		if(usertype.equals("user")) {
			ut = "1";
		}else if(usertype.equals("admin")) {
			ut = "2";
		}
		ReaderDao rdd = new ReaderDaoImpl();
		Reader rd = new Reader();
		rd.setReaderId(id);
		rd.setReaderType(ut);
		rd.setReaderName(name);
		rd.setReaderPwd(pwd);
		rd.setReaderDept(dept);
		rd.setReaderPhone(phone);
		if (rdd.register(rd)) {
			response.sendRedirect("login.jsp");
		}else {
			response.sendRedirect("register.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
