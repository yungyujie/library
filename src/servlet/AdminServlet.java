package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDao;
import dao.BookDao;
import dao.ReaderDao;
import dao.impl.AdminDaoImpl;
import dao.impl.BookDaoImpl;
import dao.impl.ReaderDaoImpl;
import model.Book;
import model.Reader;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("utf-8");
		String action = request.getParameter("action");
		try {
			if(action.equals("login")) {
				login(request,response);
			}else if(action.equals("userList")) {
				userList(request,response);
			}else if(action.equals("sendId")) {
				sendId(request,response);
			}else if(action.equals("readerReset")) {
				readerReset(request,response);
			}else if(action.equals("delUser")) {
				delUser(request,response);
			}else if(action.equals("addAdmin")) {
				addAdmin(request,response);
			}else if(action.equals("addBook")) {
				addBook(request,response);
			}else if(action.equals("checkBookList")) {
				checkBookList(request,response);
			}else if(action.equals("checkBook")) {
				checkBook(request,response);
			}else if(action.equals("disPassBook")) {
				disPassBook(request,response);
			}else if(action.equals("returnBookList")) {
				returnBookList(request,response);
			}else if(action.equals("passReturn")) {
				passReturn(request,response);
			}else if(action.equals("bookList")) {
				bookList(request,response);
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//列出图书列表
	private void bookList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub		
		BookDao bd = new BookDaoImpl();		
		HttpSession session = request.getSession();
		session.setAttribute("bookList", bd.queryAllBook());
		response.sendRedirect("admin-bookList.jsp");
	}

	//确认还书
	private void passReturn(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String bid = request.getParameter("bid");
		String uid = request.getParameter("uid");
		AdminDao ad = new AdminDaoImpl();
		if(ad.passReturn(bid, uid) && ad.addBook(bid)&&ad.delBorrow(bid, uid)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
		
	}

	//审核还书列表
	private void returnBookList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		AdminDao ad = new AdminDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("returnList", ad.queryReturnBookList());
		response.sendRedirect("admin-returnBook.jsp");
		
	}

	//取消借阅
	private void disPassBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String bid = request.getParameter("bid");
		String uid = request.getParameter("uid");
		AdminDao ad = new AdminDaoImpl();
		if(ad.disBorrowBook(bid, uid)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
		
	}

	//通过审核借阅的图书
	private void checkBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String bid = request.getParameter("bid");
		String uid = request.getParameter("uid");
		AdminDao ad = new AdminDaoImpl();
		if(ad.borrowBook(bid, uid) && ad.minusBook(bid)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//审核借书列表
	private void checkBookList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		AdminDao ad = new AdminDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("checkList", ad.queryCheckBookList());
		response.sendRedirect("admin-checkBook.jsp");
	}

	//添加图书操作
	private void addBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String bid = request.getParameter("bid");
	 	String type = request.getParameter("type");
	 	String bname = request.getParameter("bname");
	 	String author = request.getParameter("author");
	 	String publish = request.getParameter("publish");
	 	String unitPrice = request.getParameter("unitPrice");
	 	int count = Integer.parseInt(request.getParameter("count"));
	 	Book bk = new Book();
	 	bk.setISBN(bid);
	 	bk.setTypeName(type);
	 	bk.setBookName(bname);
	 	bk.setAuthor(author);
	 	bk.setPublish(publish);
	 	bk.setUnitPrice(unitPrice);
	 	bk.setCount(count);
	 	AdminDao ad = new AdminDaoImpl();
	 	if(ad.addBook(bk)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//将用户设为管理员
	private void addAdmin(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		AdminDao ad = new AdminDaoImpl();
		if(ad.addAdminById(id)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//删除用户
	private void delUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		AdminDao ad = new AdminDaoImpl();
		if(ad.delUserById(id)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//修改用户信息
	private void readerReset(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String uid = request.getParameter("uid");
		String name = request.getParameter("username");
		String dept = request.getParameter("dept");
		int phone = Integer.parseInt(request.getParameter("phone"));
		ReaderDao rd = new ReaderDaoImpl();
		if(rd.reset(uid, name, dept, phone)) {
			response.sendRedirect("true.jsp");
		}else {
			response.sendRedirect("false.jsp");
		}
	}

	//传递用户信息
	private void sendId(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String dept = request.getParameter("dept");
		int phone = Integer.parseInt(request.getParameter("phone"));
		Reader rd = new Reader();
		rd.setReaderId(id);
		rd.setReaderName(name);
		rd.setReaderDept(dept);
		rd.setReaderPhone(phone);
		HttpSession session = request.getSession();
		session.setAttribute("rId", rd);
		response.sendRedirect("admin-resetUser.jsp");
	}

	//显示用户列表
	private void userList(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		AdminDao ad = new AdminDaoImpl();
		HttpSession session = request.getSession();
		session.setAttribute("userList", ad.queryUserList());
		response.sendRedirect("userList.jsp");
		
	}

	//管理员登录
	private void login(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		String pwd = request.getParameter("password");
		AdminDao ad = new AdminDaoImpl();
		Reader reader = ad.getReaderByIdAndPwd(id, pwd);
		if(reader != null) {
			HttpSession session = request.getSession();
			session.setAttribute("admin", reader);
			response.sendRedirect("admin-home.jsp");
		}else {
			response.sendRedirect("admin-login.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
