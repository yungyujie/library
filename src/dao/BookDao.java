package dao;

import java.sql.SQLException;
import java.util.List;

public interface BookDao {
	//查询所有图书信息
	List queryAllBook() throws SQLException ;
	//订阅图书通过用户ID,图书ID
	boolean bookingById(String uid , String bid) throws SQLException ;
	//查询图书信息通过图书名字
	List queryBookByName(String name) throws SQLException ;
	//查询借书信息通过读者ID
	List queryBorrowById(String uid) throws SQLException ;
	//查询正在阅读的图书的信息通过读者ID
	List queryReadingListByid(String uid) throws SQLException ;
	//续期图书通过用户ID,图书ID
	boolean renewById(String uid , String bid) throws SQLException ;
	//搜索书评
	List searchReview(String name) throws SQLException ;
}
