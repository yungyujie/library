package dao;

import java.sql.SQLException;
import java.util.List;

import model.Book;
import model.Reader;

public interface AdminDao {
	//管理员登录
	Reader getReaderByIdAndPwd( String id , String pwd ) throws SQLException ;
	//显示用户列表
	List queryUserList() throws SQLException ;
	//删除用户通过ID
	boolean delUserById(String id) throws SQLException ;
	//将用户设为管理员
	boolean addAdminById(String id) throws SQLException ;
	//添加图书
	boolean addBook(Book bk) throws SQLException ;
	//查询借书待审核列表
	List queryCheckBookList() throws SQLException ;
	//查询还书待审核列表
	List queryReturnBookList() throws SQLException ;
	//审核通过借阅图书通过ID
	boolean borrowBook(String bid , String uid) throws SQLException ;
	//审核不通过借阅图书通过ID
	boolean disBorrowBook(String bid , String uid) throws SQLException ;
	//减少一本图书
	boolean minusBook(String bid) throws SQLException ;
	//增加一本图书
	boolean addBook(String bid) throws SQLException ;
	//审核通过归还图书通过ID
	boolean passReturn(String bid , String uid) throws SQLException ;
	//删除借书表的数据
	boolean delBorrow(String bid , String uid) throws SQLException ;
}
