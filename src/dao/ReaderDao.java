package dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import model.Reader;

public interface ReaderDao {
	//注册方法
	boolean register(Reader reader) throws SQLException ;
	//登录方法
	Reader getReaderByIdAndPwd( String id , String pwd ) throws SQLException ;
	//用户还书
	boolean returnByid(String uid , String bid) throws SQLException;
	//用户查看还书状态通过id
	List queryReturnStateById(String uid) throws SQLException;
	//用户查看一环图书通过id
	List queryReturnedById(String uid) throws SQLException ;
	//列出个人信息通过id
	Reader getReaderById(String id) throws SQLException ;
	//修改个人信息
	boolean reset(String id , String name , String dept , int phone) throws SQLException ;
	//修改密码
	boolean resetPwd(String uid , String pass , String newPass) throws SQLException ;
	//注销用户通过ID和密码
	boolean delReaderByIdAndPwd(String uid , String pass) throws SQLException ;
	//用户写书评
	boolean review(String bid , String uid , int grade , String desc) throws SQLException ;
	//查询所有评论
	List queryAllReview() throws SQLException ;
	//查询书评，通过ID
	List queryReviewById(String id) throws SQLException;
}
