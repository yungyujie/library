<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  </head>
  
  <body>
<%
	Reader reader = (Reader)session.getAttribute("reader");
%>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>IBSN</th>
            <th>读书名称</th>
            <th>作者</th>
            <th>借书日期</th>
            <th>还书日期</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
        <%
        	List list = (List)session.getAttribute("state");
        	if(list == null){
        %>
        null
        <%
        	}else{
        		for(int i = 0 ; i < list.size() ; i++){
        			Reading rd = (Reading)list.get(i);        		
        %>
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td><%=rd.getISBN() %></td>
            <td><%=rd.getBookName() %></td>
            <td><%=rd.getAuthor() %></td>
            <td><%=rd.getBorrowDate() %></td>
            <td><%=rd.getReturnDate() %></td>
            <td class="td-manage">
              <a title="我要续期"  onclick="x_admin_show('编辑','order-view.html')" href="ReaderServlet?action=renew&uid=<%=reader.getReaderId()%>&bid=<%=rd.getISBN()%>">              	
                <i class="layui-icon">&#xe63c;</i>我要续期
              </a>
               <a title="我要还书" style="margin-left: 20px" onclick="x_admin_show('编辑','order-view.html')"  href="ReaderServlet?action=return&uid=<%=reader.getReaderId()%>&bid=<%=rd.getISBN()%>">              	
                <i class="layui-icon">&#xe618;</i>我要还书
              </a>
            </td>
          </tr>
          <%
        		}
        	}
          %>
        </tbody>
      </table>
    </div>   
  </body>
</html>