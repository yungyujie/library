<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>用户注册</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
	<link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>

</head>
	<body class="login-bg">
	    <div class="login">
	        <div class="message">用户注册</div>
	        <div id="darkbannerwrap"></div>
	        <form method="post" class="layui-form" action="ReaderServlet?action=register">
	            <input name="userid" placeholder="用户ID"  type="text" lay-verify="required" class="layui-input" >
	            <hr class="hr15">
				<select id="usertype" name="type">
					<option value="user">用户</option>
					<option value="admin">管理员</option>	
				</select><br>
	            <hr class="hr15">
	            <input name="username" placeholder="用户姓名"  type="text" lay-verify="required" class="layui-input" >
	            <hr class="hr15">
	            <input name="password" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
	            <hr class="hr15">
	            <input name="dept" placeholder="院系部门"  type="text" lay-verify="required" class="layui-input" >
	            <hr class="hr15">
	            <input name="phone" placeholder="手机号"  type="text" lay-verify="required" class="layui-input" >
	            <hr class="hr15">
	            <input value="注册" lay-submit lay-filter="register" style="width:100%;" type="submit">
	            <hr class="hr20" >
	        </form>
	    </div>
	</body>
</html>