<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  </head>
  
  <body>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>用户ID</th>
            <th>用户姓名</th>
            <th>用户系别</th>
            <th>用户电话</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
	        <%
	        	List list = (List)session.getAttribute("userList");
	        	if(list == null){
	        %>
	        null
	        <%
	        	}else{
	        		for(int i = 0 ; i < list.size() ; i++){
	        			Reader rd = (Reader)list.get(i);        		
	        %>         	
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td><%=rd.getReaderId() %></td>
            <td><%=rd.getReaderName() %></td>
            <td><%=rd.getReaderDept() %></td>
            <td><%=rd.getReaderPhone() %></td>
            <td class="td-manage">
              <a title="修改信息" href="AdminServlet?action=sendId&id=<%=rd.getReaderId() %>&name=<%=rd.getReaderName() %>&dept=<%=rd.getReaderDept() %>&phone=<%=rd.getReaderPhone() %>">              	
                <i class="layui-icon">&#xe618;</i>修改信息
              </a>
              <a title="删除用户" style="margin-left: 20px;" href="AdminServlet?action=delUser&id=<%=rd.getReaderId() %>">
                <i class="layui-icon">&#xe640;</i>删除用户
              </a>
              <a title="设为管理员" style="margin-left: 20px;" href="AdminServlet?action=addAdmin&id=<%=rd.getReaderId() %>">
                <i class="layui-icon">&#xe642;</i>设为管理员
              </a>
            </td>
          </tr>
          <%
        		}
        	}
          %>
        </tbody>
      </table>
    </div>   
  </body>
</html>