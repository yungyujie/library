<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>在线书房</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <style type="text/css">
    .layui-table td, .layui-table th{
    	width: 20%;
    }
    </style>
  </head>
  
  <body>
    <div class="x-body">
<!--       <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" action="ReaderServlet?action=searchReview" method="post" >
          <input type="text" name="bookName"  placeholder="请输入图书名称" autocomplete="off" class="layui-input">
         <button class="layui-btn"  lay-submit="" lay-filter="sreach" type="submit"><i class="layui-icon">&#xe615;</i></button> 
         <input value="搜索" lay-submit  style="width:10%;" type="submit">
        </form -->
      </div>
      <table class="layui-table">
        <thead>
          <tr>
            <th>评论用户</th>
            <th>IBSN</th>
            <th>图书名称</th>            
            <th>评分</th>
            <th>书评</th>
            </tr>
        </thead>
        <tbody>
        <%
        	List list = (List)session.getAttribute("myRe");
        	if(list == null){
        %>
        null
        <%
        	}else{
                		for(int i = 0 ; i < list.size() ; i++){
                			BookReview re = (BookReview) list.get(i);
        %>
          <tr>
            <td><%=re.getReaderName() %></td>
            <td><%=re.getISBN() %></td>
            <td><%=re.getBookName() %></td>
            <td><%=re.getGrade() %> 分</td>
            <td><%=re.getTalk() %></td>            
          </tr>
          <%
        		}
        	}
          %>
        </tbody>
      </table>
  </body>
</html>