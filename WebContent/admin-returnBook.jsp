<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  </head>
  
  <body>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>IBSN</th>
            <th>图书名称</th>
            <th>读者ID</th>
            <th>读者名称</th>
            <th>还书日期</th>
            <th>当前状态</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
            <%
	        	List list = (List)session.getAttribute("returnList");
	        	if(list == null){
	        %>
	        null
	        <%
	        	}else{
	        		for(int i = 0 ; i < list.size() ; i++){
	        			BookCheck bc = (BookCheck)list.get(i);        		
	        %> 
          <tr>
            <td><%=bc.getISBN() %></td>
            <td><%=bc.getBookName() %></td>
            <td><%=bc.getReaderID() %></td>
            <td><%=bc.getReaderName() %></td>
            <td><%=bc.getReturnDate() %></td>
            <td><%=bc.getState() %></td>
            <td>
            	<a title="通过审核" href="AdminServlet?action=passReturn&bid=<%=bc.getISBN() %>&uid=<%=bc.getReaderID() %>">              	
                <i class="layui-icon">&#xe618;</i>通过审核
        		</a>
            </td>            
          </tr>
          <%
        		}
        	}
          %>
        </tbody>
      </table>
    </div>   
  </body>
</html>