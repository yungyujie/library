<%@page import="model.*"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>在线书房</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  </head>
  
  <body>
<%
	Reader reader = (Reader)session.getAttribute("reader");
%>
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" action="ReaderServlet?action=search" method="post" >
          <input type="text" name="bookName"  placeholder="请输入图书名称" autocomplete="off" class="layui-input">
         <button class="layui-btn"  lay-submit="" lay-filter="sreach" type="submit"><i class="layui-icon">&#xe615;</i></button> 
         <!-- <input value="搜索" lay-submit  style="width:10%;" type="submit"> -->
        </form>
      </div>
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>IBSN</th>
            <th>图书类别</th>
            <th>读书名称</th>
            <th>作者</th>
            <th>出版社</th>
            <th>图书单价</th>
            <th>剩余数量</th>
            <th >操作</th>
            </tr>
        </thead>
        <tbody>
        <%
        	List list = (List)session.getAttribute("bookList");
        	if(list == null){
        %>
        null
        <%
        	}else{
        		for(int i = 0 ; i < list.size() ; i++){
        			Book book = (Book) list.get(i);        		
        %>
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td><%= book.getISBN() %></td>
            <td><%= book.getTypeName() %></td>
            <td><%= book.getBookName() %></td>
            <td><%= book.getAuthor() %></td>
            <td><%= book.getPublish() %></td>
            <td><%= book.getUnitPrice() %></td>
            <td><%= book.getCount() %></td>
            <td class="td-manage">
              <a title="我要预约" href="ReaderServlet?action=booking&uid=<%=reader.getReaderId()%>&bid=<%=book.getISBN()%>">              	
                <i class="layui-icon">&#xe63c;</i>我要预约
              </a>
            </td>
          </tr>
          <%
        		}
        	}
          %>
        </tbody>
      </table>
  </body>

</html>