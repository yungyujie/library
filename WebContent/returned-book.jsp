<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  </head>
  
  <body>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>IBSN</th>
            <th>图书类别</th>
            <th>读书名称</th>
            <th>作者</th>
            <th>出版社</th>
            <th>图书单价</th>
            <th>剩余数量</th>
            <th >当前状态</th>
            </tr>
        </thead>
        <tbody>
           <%
        	List list = (List)session.getAttribute("returnedBook");
        	if(list == null){
        %>
        null
        <%
        	}else{
        		for(int i = 0 ; i < list.size() ; i++){
        			BorrowBook bb = (BorrowBook)list.get(i);        		
        %>
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td><%= bb.getISBN() %></td>
            <td><%= bb.getTypeName() %></td>
            <td><%= bb.getBookName() %></td>
            <td><%= bb.getAuthor() %></td>
            <td><%= bb.getPublish() %></td>
            <td><%= bb.getUnitPrice() %></td>            
            <td><%= bb.getCount() %></td>
            <td><%= bb.getState() %></td>
          </tr>
          <%
        		}
        	}
          %>
        </tbody>
      </table>
    </div>   
  </body>
</html>