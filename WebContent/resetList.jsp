<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
  </head>
  
  <body>
<%
	Reader reader = (Reader)session.getAttribute("resetList");
%>
    <div class="x-body">      
      <table class="layui-table">
        <thead>
          <tr>
            <th>
              <div class="layui-unselect header layui-form-checkbox" lay-skin="primary"><i class="layui-icon">&#xe605;</i></div>
            </th>
            <th>用户账号</th>
            <th>用户名称</th>
            <th>用户系别</th>
            <th>用户电话</th>
            <th>操作</th>
            </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="layui-unselect layui-form-checkbox" lay-skin="primary" data-id='2'><i class="layui-icon">&#xe605;</i></div>
            </td>
            <td><%=reader.getReaderId() %></td>
            <td><%=reader.getReaderName() %></td>
            <td><%=reader.getReaderDept() %></td>
            <td><%=reader.getReaderPhone() %></td>
            <td class="td-manage">
              <a title="修改信息"  href="readerReset.jsp">              	
                <i class="layui-icon">&#xe618;</i>修改信息
              </a>
              <a title="修改密码" style="margin-left: 20px;"  href="resetPwd.jsp">
                <i class="layui-icon">&#xe63c;</i>修改密码
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>   
  </body>
</html>