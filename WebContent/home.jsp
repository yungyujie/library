<%@page import="model.Reader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>后台系统管理</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/font.css">
	<link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>

</head>
<body>
<%
	Reader reader = (Reader)session.getAttribute("reader");
%>
    <!-- 顶部开始 -->
    <div class="container">
        <div class="logo"><a href="index.html">图书管理系统</a></div>
        <div class="left_open">
            <i title="展开左侧栏" class="iconfont">&#xe699;</i>
        </div>        
        <ul class="layui-nav right" lay-filter="">
        	<li class="layui-nav-item">
        		<a href="#">欢迎你：<%= reader.getReaderName() %></a>
        	</li>
        	<li class="layui-nav-item">
        		<a href="ReaderServlet?action=back">退出登录</a>
        	</li>
        </ul>
        
    </div>
     <!-- 顶部结束 -->
    <!-- 中部开始 -->
     <!-- 左侧菜单开始 -->
    <div class="left-nav">
      <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>个人管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="ReaderServlet?action=resetList&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>修改信息</cite>
                            
                        </a>
                    </li >
                    <li>
                        <a _href="deletReader.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>注销账户</cite>
                            
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe723;</i>
                    <cite>在线借书</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="ReaderServlet?action=bookList">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>在线书房</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="ReaderServlet?action=bookState&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>借书状态</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe726;</i>
                    <cite>图书归还</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="ReaderServlet?action=readingList&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>我的书单</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="ReaderServlet?action=returnState&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>待审图书</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="ReaderServlet?action=returned&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>已还图书</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6ce;</i>
                    <cite>图书评价</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="ReaderServlet?action=wantSay&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>我要评价</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="ReaderServlet?action=comReview">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>大家评价</cite>
                        </a>
                    </li>
                    <li>
                        <a _href="ReaderServlet?action=myReview&uid=<%=reader.getReaderId()%>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>历史评价</cite>
                        </a>
                    </li>                    
                </ul>
            </li>
        </ul>
      </div>
    </div>
    <!-- <div class="x-slide_left"></div> -->
    <!-- 左侧菜单结束 -->
    <!-- 右侧主体开始 -->
    <div class="page-content">
        <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
          <ul class="layui-tab-title">
            <li>我的桌面</li>
          </ul>
          <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='./welcome.jsp' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
          </div>
        </div>
    </div>
    <div class="page-content-bg"></div>
    <!-- 右侧主体结束 -->
    <!-- 中部结束 -->
    <!-- 底部开始 -->
    <div class="footer">
        <div class="copyright">Copyright ©2019 第一小组</div>  
    </div>
    <!-- 底部结束 -->
    
</body>
</html>