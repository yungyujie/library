/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : library

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-10 21:32:49
*/

SET FOREIGN_KEY_CHECKS=0;
CREATE Database library SET utf8 COLLATE utf8_general_ci ;
use library;
-- ----------------------------
-- Table structure for `book`
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `ISBN` varchar(20) NOT NULL,
  `TypeID` varchar(50) NOT NULL,
  `BookName` varchar(30) NOT NULL,
  `Author` varchar(30) DEFAULT NULL,
  `Publish` varchar(50) DEFAULT NULL,
  `UnitPrice` varchar(30) DEFAULT NULL,
  `Count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('102432', '3', '读心术', '老王', '清华大学出版社', '33', '8');
INSERT INTO `book` VALUES ('123654', '2', 'Docker核心技术', '喜羊羊', '世界图书出版社', '49', '3');
INSERT INTO `book` VALUES ('232163', '4', '盗墓笔记', '南派三叔', '中国图书出版社', '42', '5');
INSERT INTO `book` VALUES ('232342', '1', '百科大世界', '李四', '工业出版社', '73', '10');
INSERT INTO `book` VALUES ('236432', '2', 'Java程序设计', '张三', '机械工业出版社', '68', '4');
INSERT INTO `book` VALUES ('345213', '4', '鬼吹灯', '懒羊羊', '四川大学出版社', '28', '3');

-- ----------------------------
-- Table structure for `bookback`
-- ----------------------------
DROP TABLE IF EXISTS `bookback`;
CREATE TABLE `bookback` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ReaderID` varchar(50) NOT NULL,
  `ISBN` varchar(20) NOT NULL,
  `ReturnDate` datetime NOT NULL COMMENT '实际归还日期',
  `State` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bookback
-- ----------------------------
INSERT INTO `bookback` VALUES ('1', '20181001', '232342', '2019-01-09 15:57:09', '待审核');
INSERT INTO `bookback` VALUES ('2', '20181001', '232163', '2019-01-09 16:27:19', '已还');
INSERT INTO `bookback` VALUES ('3', '20181002', '232342', '2019-01-10 14:24:30', '已还');

-- ----------------------------
-- Table structure for `booktype`
-- ----------------------------
DROP TABLE IF EXISTS `booktype`;
CREATE TABLE `booktype` (
  `TypeID` varchar(50) NOT NULL,
  `TypeName` varchar(30) NOT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of booktype
-- ----------------------------
INSERT INTO `booktype` VALUES ('1', '科普');
INSERT INTO `booktype` VALUES ('2', '教育');
INSERT INTO `booktype` VALUES ('3', '心理');
INSERT INTO `booktype` VALUES ('4', '小说');

-- ----------------------------
-- Table structure for `borrowbook`
-- ----------------------------
DROP TABLE IF EXISTS `borrowbook`;
CREATE TABLE `borrowbook` (
  `ReaderID` varchar(50) NOT NULL,
  `ISBN` varchar(20) NOT NULL,
  `BorrowDate` datetime NOT NULL,
  `ReturnDate` datetime NOT NULL,
  `State` varchar(10) NOT NULL COMMENT '0失败、1成功、2预约'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of borrowbook
-- ----------------------------
INSERT INTO `borrowbook` VALUES ('20181001', '232163', '2019-01-08 18:38:55', '2019-01-30 09:43:15', '通过');
INSERT INTO `borrowbook` VALUES ('20181001', '232342', '2019-01-09 08:57:58', '2019-01-30 14:24:20', '未通过');
INSERT INTO `borrowbook` VALUES ('20181001', '236432', '2019-01-09 10:19:07', '2019-01-29 10:19:07', '待审核');
INSERT INTO `borrowbook` VALUES ('20181002', '232163', '2019-01-10 14:23:42', '2019-01-30 14:23:42', '待审核');

-- ----------------------------
-- Table structure for `reader`
-- ----------------------------
DROP TABLE IF EXISTS `reader`;
CREATE TABLE `reader` (
  `ReaderID` varchar(20) NOT NULL,
  `ReaderType` varchar(10) NOT NULL,
  `ReaderName` varchar(50) NOT NULL,
  `ReaderPwd` varchar(50) NOT NULL,
  `ReaderDept` varchar(20) NOT NULL,
  `ReaderPhone` bigint(20) NOT NULL,
  `ReaderShow` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ReaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reader
-- ----------------------------
INSERT INTO `reader` VALUES ('1001', '2', '李四一', '123', '计科系', '123', '');
INSERT INTO `reader` VALUES ('20181001', '1', '张三一', '123', '软件技术', '123', '');
INSERT INTO `reader` VALUES ('20181002', '1', '张三二', '123', '信息安全', '123456', '');
INSERT INTO `reader` VALUES ('20181003', '1', '张三三', '123', '计算机网络', '123456', '');
INSERT INTO `reader` VALUES ('20181004', '1', '张三四', '123', '数字媒体', '123', '');

-- ----------------------------
-- Table structure for `readertype`
-- ----------------------------
DROP TABLE IF EXISTS `readertype`;
CREATE TABLE `readertype` (
  `TypeID` varchar(20) NOT NULL,
  `TypeName` varchar(20) NOT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of readertype
-- ----------------------------
INSERT INTO `readertype` VALUES ('1', '用户');
INSERT INTO `readertype` VALUES ('2', '管理员');

-- ----------------------------
-- Table structure for `review`
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ISBN` varchar(20) NOT NULL,
  `ReaderID` varchar(50) NOT NULL,
  `Grade` int(10) NOT NULL,
  `Talk` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES ('1', '232163', '20181002', '4', '好好好！');
INSERT INTO `review` VALUES ('2', '232163', '20181001', '4', '很棒');
INSERT INTO `review` VALUES ('3', '236432', '20181003', '3', '好评');
INSERT INTO `review` VALUES ('4', '232163', '20181001', '4', '123');
INSERT INTO `review` VALUES ('5', '232163', '20181001', '5', '测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符测试多字符');
